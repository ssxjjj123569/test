#!/bin/bash
#
file=/etc/passwd
i=0

while read LINE;do
  [ `echo $LINE | awk 'BEGIN{FS=":"}{print $NF}'` == '/sbin/nologin' ] && echo $LINE | awk -F ":" '{print $1}' && let i++
 [ $i -eq 5 ] && break
done < $file
